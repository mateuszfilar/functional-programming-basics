import Vue from "vue";
import Router from "vue-router";

import Home from "./views/Home.vue";
import ImperativeVsDeclarative from "./views/ImperativeVsDeclarative";
import PureFunction from "./views/PureFunction";
import PointFree from "./views/PointFree";
import Curry from "./views/Curry";
import Compose from "./views/Compose";

import Exercises from "./views/Exercises";
import Exercise1 from "./components/Exercise1";
import Exercise2 from "./components/Exercise2";
import Exercise3 from "./components/Exercise3";
import Exercise4 from "./components/Exercise4";
import Exercise5 from "./components/Exercise5";

import PracticalUseCases from "./views/PracticalUseCases";

import MoreExercises from "./views/MoreExercises";
import Exercise6 from "./components/Exercise6";
import Exercise7 from "./components/Exercise7";
import Exercise8 from "./components/Exercise8";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/imperative",
      component: ImperativeVsDeclarative
    },
    {
      path: "/pure",
      component: PureFunction
    },
    {
      path: "/pointfree",
      component: PointFree
    },
    {
      path: "/currying",
      component: Curry
    },
    {
      path: "/compose",
      component: Compose
    },
    {
      path: "/practical-use-cases",
      component: PracticalUseCases
    },
    {
      path: "/example",
      component: Exercises,
      children: [
        {
          path: "/exercise/1",
          component: Exercise1
        },
        {
          path: "/exercise/2",
          component: Exercise2
        },
        {
          path: "/exercise/3",
          component: Exercise3
        },
        {
          path: "/exercise/4",
          component: Exercise4
        },
        {
          path: "/exercise/5",
          component: Exercise5
        }
      ]
    },
    {
      path: "/exercises",
      component: MoreExercises,
      children: [
        {
          path: "/exercise/6",
          component: Exercise6
        },
        {
          path: "/exercise/7",
          component: Exercise7
        },
        {
          path: "/exercise/8",
          component: Exercise8
        }
      ]
    }
  ]
});
