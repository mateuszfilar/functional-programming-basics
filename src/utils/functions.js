export function curry(fn) {
  return (...args) => {
    if (args.length === 0) {
      throw Error("EMPTY INVOCATION");
    }
    if (args.length >= fn.length) {
      return fn(...args);
    }
    return curry(fn.bind(null, ...args));
  };
}

export const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);

// compose :: ((a -> b), (b -> c),  ..., (y -> z)) -> a -> z
export const compose = (...fns) => (...args) =>
  fns.reduceRight((res, fn) => [fn.call(null, ...res)], args)[0];

// flip :: (a -> b -> c) -> b -> a -> c
export const flip = curry((fn, a, b) => fn(b, a));

// append :: String -> String -> String
export const append = flip(concat);

// concat :: String -> String -> String
export const concat = curry((a, b) => a.concat(b));

// eq :: Eq a => a -> a -> Boolean
export const eq = curry((a, b) => a === b);

// filter :: (a -> Boolean) -> [a] -> [a]
export const filter = curry((fn, xs) => xs.filter(fn));

// forEach :: (a -> ()) -> [a] -> ()
export const forEach = curry((fn, xs) => xs.forEach(fn));

// head :: [a] -> a
export const head = xs => xs[0];

// intercalate :: String -> [String] -> String
export const intercalate = curry((str, xs) => xs.join(str));

// join :: Monad m => m (m a) -> m a
export const join = m => m.join();

// last :: [a] -> a
export const last = xs => xs[xs.length - 1];

// map :: Functor f => (a -> b) -> f a -> f b
export const map = curry((fn, f) => f.map(fn));

// match :: RegExp -> String -> Boolean
export const match = curry((re, str) => re.test(str));

// prop :: String -> Object -> a
export const prop = curry((p, obj) => obj[p]);

// reduce :: (b -> a -> b) -> b -> [a] -> b
export const reduce = curry((fn, zero, xs) => xs.reduce(fn, zero));

// replace :: RegExp -> String -> String -> String
export const replace = curry((re, rpl, str) => str.replace(re, rpl));

// reverse :: [a] -> [a]
export const reverse = x =>
  Array.isArray(x)
    ? x.reverse()
    : x
        .split("")
        .reverse()
        .join("");

// sortBy :: Ord b => (a -> b) -> [a] -> [a]
export const sortBy = curry((fn, xs) => {
  return xs.sort((a, b) => {
    if (fn(a) === fn(b)) {
      return 0;
    }

    return fn(a) > fn(b) ? 1 : -1;
  });
});

// split :: String -> String -> [String]
export const split = curry((sep, str) => str.split(sep));

// take :: Number -> [a] -> [a]
export const take = curry((n, xs) => xs.slice(0, n));

// toLowerCase :: String -> String
export const toLowerCase = s => s.toLowerCase();

// toString :: a -> String
export const toString = String;

// toUpperCase :: String -> String
export const toUpperCase = s => s.toUpperCase();
